import { createAction } from '@ngrx/store';

export const increment = createAction('[Display Component] Increment');
export const decrement = createAction('[Display Component] Decrement');


