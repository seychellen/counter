import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { ButtonComponent } from './button/button.component';
import { DisplayComponent } from './display/display.component';
import { counterReducer } from "./store/counter.reducer";

@NgModule({
  declarations: [
    AppComponent,
    ButtonComponent,
    DisplayComponent
  ],
  imports: [
    BrowserModule,
    // wir hängt das mit dem Store zusammen? Auf alle Fälle muss "count"
    // dem "count" von Store<{ count: number } übereinstimmen!
    StoreModule.forRoot({ count: counterReducer})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
