import {Component, Input} from '@angular/core';
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";
import {decrement, increment} from "./store/counter.actions";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  @Input()  count$: Observable<number>;

  // Hier wird die Struktur des Stores festgelegt:
  constructor(private store: Store<{ count: number }>) {
    this.count$ = store.select('count')
  }

  title = 'Counter';

  onClickEvent(inc: boolean) {
    if (inc) {
      this.store.dispatch(increment())
    }
    else {
      this.store.dispatch(decrement())
    }
  }
}
